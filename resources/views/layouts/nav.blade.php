<!-- Heading -->
<div class="columns">
  <div class="column has-text-centered">
    <div class="page-header">
      <h1 class="title is-2"><a class="main-title" href="/" style="color:white"><i class="fa fa-gamepad" aria-hidden="true"></i> Game Reviewer</a></h1>
    </div>
  </div>
</div>
<!-- Navigation -->
<div class="columns">
  <div class="column has-text-centered tab-col">
    <div class="tabs is-centered">
      <ul>
        <li><a href="{{ url('/games/calendar')}}"><i class="fa fa-calendar" aria-hidden="true"></i> Upcoming Games</a></li>
        <li><a href="{{ url('/games/all')}}"><i class="fa fa-search" aria-hidden="true"></i> View all Games</a></li>
        <li><a href="{{ url('/news') }}"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Recent News</a></li>
        <li><a href="{{ url('/threads') }}"><i class="fa fa-comments-o" aria-hidden="true"></i> Forums</a></li>
        @if (Auth::check())
            <li><a href="/users/{{ Auth::user()->id }}"><i class="fa fa-user" aria-hidden="true"></i> My Account</a></li>
            <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
        @else
            <li><a href="{{ url('/login') }}"><i class="fa fa-key" aria-hidden="true"></i> Login</a></li>
            <li><a href="{{ url('/register') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Register</a></li>
        @endif
      </ul>
    </div>
  </div>
</div>
