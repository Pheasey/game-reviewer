<div class="video-wrapper">
  <iframe width="640" height="360" max-width="100%" src="https://www.youtube.com/embed/{{ $game->youtube }}" frameborder="0" allowfullscreen></iframe>
</div>
<p class="has-text-centered">View more {{ $game->title }} videos <a href="https://www.youtube.com/results?search_query={{ $game->title }}">here</a>.</p>
