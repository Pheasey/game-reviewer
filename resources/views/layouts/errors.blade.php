@if (count($errors))
  <br>
  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach
@endif
