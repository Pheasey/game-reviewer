<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        <strong>Game Reviewer</strong> by <a href="#">Team Achieve</a>.
        <br>
        Created using <a href="https://laravel.com/">Laravel</a> PHP Framework and <a href="http://bulma.io/">Bulma.io</a> CSS.
      </p>
      <p>
        <a class="icon" href="https://gitlab.com/Pheasey/game-reviewer/">
          <i class="fa fa-gitlab"></i>
        </a>
      </p>
    </div>
  </div>
</footer>
