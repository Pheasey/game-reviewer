<h4 class="is-title">This month..</h4>

@foreach ($recentGames as $game)

<article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <i class="fa fa-calendar-minus-o" aria-hidden="true"></i> {{ $game->formattedDate() }}<br>
    </p>
  </figure>
  <div class="media-content">
    <i class="fa fa-gamepad" aria-hidden="true"></i>
    <a href="/games/{{ $game->id }}">{{ $game->title }}</a><br>
  </div>
</article>

@endforeach

<h3 class="is-title">Recent Threads</h3>

@foreach ($recentThreads as $thread)

  <article class="media">
    <figure class="media-left">
      <p class="image is-64x64">
        <img src="https://robohash.org/{{ $thread->user->id }}.png">
      </p>
    </figure>
    <div class="media-content">
      <div class="content">
        <p>
          <strong><a href="/users/{{$thread->user->id}}">{{ $thread->user->name }}</a></strong>
          <br>
          <a href="/threads/{{$thread->id}}">{{ $thread->title }}</a>
          <br>
          For the game <i>{{ $thread->game->title }}</i>
          <br>
        </p>
      </div>
    </div>
  </article>

@endforeach

{{-- <article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <img src="http://bulma.io/images/placeholders/128x128.png">
    </p>
  </figure>
  <div class="media-content">
    <div class="content">
      <p>
        <strong>Barbara Middleton</strong>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros lacus, nec ultricies elit blandit non.
      </p>
    </div>
  </div>
</article> --}}
{{-- More Fake Upcoming Games.. --}}
{{-- <article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <img src="http://bulma.io/images/placeholders/128x128.png">
    </p>
  </figure>
  <div class="media-content">
    <div class="content">
      <p>
        <strong>Barbara Middleton</strong>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros lacus, nec ultricies elit blandit non.
      </p>
    </div>
  </div>
</article>
<article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <img src="http://bulma.io/images/placeholders/128x128.png">
    </p>
  </figure>
  <div class="media-content">
    <div class="content">
      <p>
        <strong>Barbara Middleton</strong>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros lacus, nec ultricies elit blandit non.
      </p>
    </div>
  </div>
</article>
<article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <img src="http://bulma.io/images/placeholders/128x128.png">
    </p>
  </figure>
  <div class="media-content">
    <div class="content">
      <p>
        <strong>Barbara Middleton</strong>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros lacus, nec ultricies elit blandit non.
      </p>
    </div>
  </div>
</article>
<article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <img src="http://bulma.io/images/placeholders/128x128.png">
    </p>
  </figure>
  <div class="media-content">
    <div class="content">
      <p>
        <strong>Barbara Middleton</strong>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros lacus, nec ultricies elit blandit non.
      </p>
    </div>
  </div>
</article> --}}
{{-- End of fake games. --}}
