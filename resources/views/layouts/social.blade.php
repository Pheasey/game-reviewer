<nav class="level is-pulled-right">
  <div class="level-right">
    <a class="level-item">
      <a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </a>&nbsp;&nbsp;
    <a class="level-item">
      <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fgamereviewer.co.uk&layout=button&size=small&mobile_iframe=true&width=58&height=20&appId" width="58" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
    </a>
    <a class="level-item">
      <div class="g-plus" data-action="share" data-annotation="none"></div>
    </a>
  </div>
</nav>
<br>
