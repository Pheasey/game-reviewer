<section class="light-bg">
  <article>
    <h2>Comments</h2>
    <div class="row">
      <div class="col">
        <table class="forum-table">
          <tr>
            <th>Thread Title</th>
            <th>Comments</th>
            <th>Date Created</th>
            <th>Last Updated</th>
          </tr>
          <tr>
            <td>Does anyone play this game?</td>
            <td>3</td>
            <td>Jan 12th</td>
            <td>Jan 18th</td>
          </tr>
          <tr>
            <td>Secret Easter Egg</td>
            <td>2</td>
            <td>Dec 29th</td>
            <td>Jan 2nd</td>
          </tr>
          <tr>
            <td>Last Level Tutorial</td>
            <td>8</td>
            <td>Nov 30th</td>
            <td>Dec 19th</td>
          </tr>
        </table>
      </div>
    </div>
  </article>
</section>
