<form method="GET" action="/games/search/" class="control has-addons">
  <input class="input" type="text" id="term" name="term" placeholder="Search for a game">
  <button class="button">Search</button>
</form>
