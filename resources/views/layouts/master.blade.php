<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Game Reviews</title>
		<link rel="stylesheet" href="./css/hydra.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Roboto:300,400" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	</head>
	<body>
		<div class="wrapper">
			<section class="dark-bg">
				<article>
					<div class="row">
						<div class="col center-h between">
							<h1><i class="fa fa-gamepad" aria-hidden="true"></i> Game Reviewer</h1>
							<ul class="nav-bar between center-v center-t">
								<li><i class="fa fa-newspaper-o" aria-hidden="true"></i> News</li>
								<li><i class="fa fa-calendar" aria-hidden="true"></i> Upcoming Releases</li>
								<li><i class="fa fa-search" aria-hidden="true"></i>Search</li>
								<li><i class="fa fa-user" aria-hidden="true"></i> My Account</li>
							</ul>
						</div>
					</div>
				</article>
			</section>
			<section class="dark-bg car-bg center-vh center-t">
				<article>
					<h1>Heading</h1>
					<p>Paragraph</p>
				</article>
			</section>
			<section class="light-bg">
				<article>
					<div class="row">
						<div class="col">
							<h3>Heading</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
						<div class="col">
							<h3>Heading</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div class="row">
						<div class="col">

              @yield('content')

            </div>
					</div>
				</article>
			</section>
			<section class="light-bg">
				<article>
					<h2>News</h2>
					<div class="row">
						<div class="col">
							<h3>Heading</h3>
							<a class="btn">View</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
						<div class="col">
							<h3>Heading</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
				</article>
			</section>
			<section class="light-bg">
				<article>
					<h2>Comments</h2>
					<div class="row">
						<div class="col">
							<table class="forum-table">
								<tr>
									<th>Thread Title</th>
									<th>Comments</th>
									<th>Date Created</th>
									<th>Last Updated</th>
								</tr>
								<tr>
									<td>Does anyone play this game?</td>
									<td>3</td>
									<td>Jan 12th</td>
									<td>Jan 18th</td>
								</tr>
								<tr>
									<td>Secret Easter Egg</td>
									<td>2</td>
									<td>Dec 29th</td>
									<td>Jan 2nd</td>
								</tr>
								<tr>
									<td>Last Level Tutorial</td>
									<td>8</td>
									<td>Nov 30th</td>
									<td>Dec 19th</td>
								</tr>
							</table>
						</div>
					</div>
				</article>
			</section>
			<section class="dark-bg">
				<article>
					<div class="row">
						<div class="col">
							<div class="footer-tab">
								<h4>Game Reviewer 2017</h4>
								<ul class="">
									<li>News</li>
									<li>Upcoming Releases</li>
									<li>Search</li>
									<li>My Account</li>
								</ul>
							</div>
						</div>
						<div class="col">
							<div class="footer-tab">
								<h3>Game Reviewer</h3>
								<ul class="">
									<li>News</li>
									<li>Upcoming Releases</li>
									<li>Search</li>
									<li>My Account</li>
								</ul>
							</div>
						</div>
						<div class="col">
							<div class="footer-tab">
								<h3>Game Reviewer</h3>
								<ul class="">
									<li>News</li>
									<li>Upcoming Releases</li>
									<li>Search</li>
									<li>My Account</li>
								</ul>
							</div>
						</div>
						<div class="col">
							<div class="footer-tab">
								<h3>Game Reviewer</h3>
								<ul class="">
									<li>News</li>
									<li>Upcoming Releases</li>
									<li>Search</li>
									<li>My Account</li>
								</ul>
							</div>
						</div>
					</div>
				</article>
			</section>
		</div>
	</body>
</html>
