@extends('layout')

@section('content')

  <h3 class="is-title">Editing {{ $news->title }}</h3>

  <form action="/news/{{ $news->id }}" method="POST">

    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">

    <label class="label">Title</label>
    <p class="control">
      <input class="input" id="title" name="title" type="text" value="{{ $news->title }}">
    </p>

    <label class="label">Body</label>
    <p class="control">
      <textarea class="textarea" id="body" name="body">{{ $news->body }}</textarea>
    </p>

    <p class="control">
      <button class="button is-primary">Update</button>
    </p>

  </form>

  @include('layouts.errors')

@endsection
