@extends('layout')

@section('head')
  <script src="https://apis.google.com/js/platform.js" async defer>
    {lang: 'en-GB'}
  </script>
@endsection

@section('content')

  <h3 class="is-title"><b>{{ $news->title }}</b></h3>

  {!! Markdown::parse($news->body) !!}

  <hr>
  <small>Posted {{ $news->created_at->diffForHumans() }}</small>

  @if (Auth::user() && (Auth::user()->role->name == "Admin"))
    <a class="button is-pulled-right" href="/news/{{ $news->id }}/edit">Edit</a>
  @endif

  @include('layouts.social')

@endsection
