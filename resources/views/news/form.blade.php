@extends('layout')

@section('content')

  <form action="/news/create" method="POST">

    {{ csrf_field() }}

    <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">

    <label class="label">Title</label>
    <p class="control">
      <input class="input" id="title" name="title" type="text">
    </p>

    <label class="label">Body</label>
    <p class="control">
      <textarea class="textarea" id="body" name="body"></textarea>
    </p>

    <p class="control">
      <button class="button is-primary">Create</button>
    </p>

  </form>

  @include('layouts.errors')

@endsection
