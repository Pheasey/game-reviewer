@extends('layout')

@section('content')

  <h3 class="is-title">Latest News</h3>

  @foreach ($news as $article)

    @include('news.news')

  @endforeach

  {{ $news->links() }}


  @if (Auth::user() && (Auth::user()->role->name == "Admin"))
    <a class="button" href="/news/create">Create a Post</a>
  @endif

@endsection
