<h4><b><a href="/news/{{ $article->id }}">{{ $article->title }}</a></b></h4>

{!! Markdown::parse(substr($article->body, 0, 500)) !!}

<small>Posted {{ $article->created_at->diffForHumans() }}</small>
<a class="button is-pulled-right" href="/news/{{ $article->id }}">Read More</a>

<hr>
