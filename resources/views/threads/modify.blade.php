@if (Auth::user() && (Auth::user()->role->name == "Admin"))

  <div class="card-content">

    <div class="columns">
      <div class="column">

        <form action="/threads/{{ $thread->id }}/pin" method="POST">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <button class="button is-fullwidth"><i class="fa fa-thumb-tack" aria-hidden="true"></i></button>
        </form>

      </div>
      <div class="column">

        <form action="/threads/{{ $thread->id }}/lock" method="POST">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <button class="button is-fullwidth"><i class="fa fa-lock" aria-hidden="true"></i></button>
        </form>

      </div>
      <div class="column">

        <form action="/threads/{{ $thread->id }}/hide" method="POST">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <button class="button is-fullwidth"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
        </form>

      </div>
    </div>

  </div>

@endif
