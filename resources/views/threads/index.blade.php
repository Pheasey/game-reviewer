@extends('layout')

@section('content')

  <h3 class="is-title">Recent Threads</h3>

  <table class="forum-table">
    <tr>
      <th>Game</th>
      <th>Thread Title</th>
      <th>Author</th>
      <th>Replies</th>
      <th>Created</th>
    </tr>

    @foreach ($threads as $thread)

      @include('threads.thread_more')

    @endforeach

  </table>

  {{ $threads->links() }}

@endsection
