@extends('layout')

@section('content')

  <h3 class="is-title">Creating a new thread for {{ $game->title }}</h3>

  <form method="POST" action="/threads/{{$game->id}}/new">

    {{ csrf_field() }}

    <input type="hidden" id="game_id" name="game_id" value="{{ $game->id }}">
    <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">

    <label class="label">Thread Title</label>
    <p class="control">
      <input class="input" id="title" name="title" type="text">
    </p>

    <label class="label">Body</label>
    <p class="control">
      <textarea class="textarea" id="body" name="body"></textarea>
    </p>

    <p class="control">
      <button class="button is-primary">Create</button>
    </p>

  </form>

  @include('layouts.errors')

@endsection
