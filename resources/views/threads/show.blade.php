@extends('layout')

@section('head')

  <script src="https://apis.google.com/js/platform.js" async defer>
    {lang: 'en-GB'}
  </script>

@endsection

@section('content')

  <div class="notification has-text-centered">
    <p class="is-title is-5 has-text-centered">{{ $thread->game->title }} thread, <a href="/games/{{ $thread->game->id }}">click here</a> to return to it's page.</p>
  </div>

  <div class="card">
    <header class="card-header">
      <p class="card-header-title">
        @if ($thread->pinned)
          	&nbsp; <i class="fa fa-thumb-tack" aria-hidden="true"></i>	&nbsp;
        @endif
        @if ($thread->locked)
          	&nbsp; <i class="fa fa-lock" aria-hidden="true"></i>	&nbsp;
        @endif
        @if ($thread->hidden)
          	&nbsp; <i class="fa fa-eye-slash" aria-hidden="true"></i>	&nbsp;
        @endif

        {{ $thread->title }} -  <a href="/users/{{ $thread->user->id }}"> {{ $thread->user->name }}</a>
      </p>
    </header>
    <div class="card-content">

      @if (Auth::user() && (Auth::id() == $thread->user->id) && $thread->hidden != true && $thread->locked != true)
        <a class="button is-pulled-right" href="/threads/{{ $thread->id }}/edit">Edit</a>
      @endif

      @if (!$thread->hidden)
        {!! Markdown::parse($thread->body) !!}
      @else
        <p><i>This thread was hidden.</i></p>
      @endif

    </div>

  @include('threads.modify')

  </div>

  <div class="card">
    <div class="card-content">

      @foreach ($thread->posts as $post)
        @include('posts.post')
      @endforeach

    </div>
  </div>

  @if ($thread->locked == true)

    <br>
    <div class="notification has-text-centered">
      This thread is <b>locked</b>. No further replies possible.
    </div>

  @else

    @if (Auth::check())

        @include('posts.form')

    @endif

  @endif

  @include('layouts.social')

@endsection
