<tr @if ($thread->pinned) class="pinned-thread" @endif >
  <td>
    @if ($thread->pinned)
      <span class="icon">
        <i class="fa fa-thumb-tack" aria-hidden="true"></i>
      </span>
    @endif
    @if ($thread->locked)
      <span class="icon">
        <i class="fa fa-lock" aria-hidden="true"></i>
      </span>
    @endif
    @if (!$thread->hidden)
      <a href="/threads/{{ $thread->id }}">{{ substr($thread->title, 0, 50) }}..</a></td>
    @else
      <a href="/threads/{{ $thread->id }}"><i>This thread was hidden.</i></a></td>
    @endif

  <td><a href="/users/{{$thread->user->id }}">{{ $thread->user->name }}</a></td>
  <td>{{ $thread->posts->count() }}</td>
  {{-- <td>{{ substr($thread->body, 0, 20) }}...</td> --}}
  <td>{{ $thread->created_at->diffForHumans() }}</td>
</tr>
