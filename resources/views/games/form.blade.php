@extends('layout')

@section('content')

  <h3 class="is-title">Creating a new Game</h3>

  <form action="/games/create" method="POST">

    {{ csrf_field() }}

    <label class="label">Title</label>
    <p class="control">
      <input class="input" id="title" name="title" type="text">
    </p>

    <label class="label">Release Date</label>
    <p class="control">
      <input class="input" id="release_date" name="release_date" type="date">
    </p>

    <label class="label">Video URL</label>
    <p class="control">
      <input class="input" id="youtube" name="youtube" type="text">
    </p>

    <label class="label">Website URL</label>
    <p class="control">
      <input class="input" id="website" name="website" type="text">
    </p>

    <p class="control">
      <button class="button is-primary">Create</button>
    </p>

  </form>

  @include('layouts.errors')

@endsection
