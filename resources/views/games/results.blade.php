@extends('layout')

@section('content')

  <h3 class="is-title">Search Results</h3>
  <p>{{ $games->total() }} result(s) found for '{{ $query }}'.</p>

  <nav class="panel">

    @foreach ($games as $game)

      @include('games.game')

    @endforeach

  </nav>

  {{ $games->appends(['term' => $query])->links() }}

@endsection
