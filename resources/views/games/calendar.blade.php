@extends('layout')

@section('head')

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

@endsection

@section('content')

  <h3 class="is-title">Game Calandar</h3>

  {!! $calendar->calendar() !!}
  {!! $calendar->script() !!}

  <h3 class="is-title">This Month</h3>

  <nav class="panel">

    @foreach ($thisMonth as $game)

      @include('games.game')

    @endforeach

  </nav>

  <h3 class="is-title">Last Month</h3>

  <nav class="panel">

    @foreach ($lastMonth as $game)

      @include('games.game')

    @endforeach

  </nav>

@endsection
