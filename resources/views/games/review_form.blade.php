<h4 class="is-title">Leave a Review</h4>

<form method="POST" action="/games/{{ $game->id }}/review/new">

    {{ csrf_field() }}

    <input type="hidden" id="game_id" name="game_id" value="{{ $game->id }}">
    <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">

    <label class="label">Star Rating <small>0/10</small></label>
    <p class="control">
      <input class="input" id="rating" name="rating" type="number" min="0" max="10">
    </p>

    <label class="label">Review</label>
    <p class="control">
      <textarea class="textarea" id="review" name="review"></textarea>
    </p>

    <p class="control">
      <button class="button is-primary">Create</button>
    </p>

</form>
