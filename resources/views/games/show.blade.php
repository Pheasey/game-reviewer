@extends('layout')

@section('content')

  {{-- Title --}}
  <div class="columns">
    <div class="column">
      <h1 class="is-title">

        {{ $game->title }}

      </h1>
    </div>
  </div>

  <div class="columns">
    {{-- Game Info --}}
    <div class="column is-one-third game-info">

      @include('games.star_rating')

      @include('games.details.genres')

      @include('games.details.release')

      @include('games.details.developer')

      @include('games.details.platforms')

      @include('games.details.descriptors')

      <p><b>Official Website: </b>
        <a href="{{ $game->website }}">
          <i class="fa fa-external-link" aria-hidden="true"></i></p>
        </a>
      </p>

    </div>

    {{-- Game Video --}}
    <div class="column">
      @include('layouts.youtube')
    </div>

  </div>

  {{-- Threads --}}
  <div class="columns">
    <div class="column">

      <h2 class="is-title is-2">Latest Reviews</h2>

      @include('games.reviews')

      @if (Auth::check() && !$game->reviews->contains(Auth::user()->id))

          @include('games.review_form')

      @elseif (Auth::check() && $game->reviews->contains(Auth::user()->id))

        Want to update your review? <a href="/games/{{$game->id}}/review/update">Click here </a> to update it.

      @endif

      <h2 class="is-title is-2">Threads</h2>

      <table class="forum-table">
        <tr>
          <th>Thread Title</th>
          <th>Author</th>
          <th>Replies</th>
          <th>Created</th>
        </tr>

        @foreach ($game->threads->sortByDesc('created_at')->where('pinned', '=', true) as $thread)
          @include('threads.thread')
        @endforeach

        @foreach ($game->threads->sortByDesc('created_at')->where('pinned', '!=', true) as $thread)
          @include('threads.thread')
        @endforeach

      </table>

      @if ($game->threads->count() == 0)

        <div class="notification has-text-centered">
          No threads found, be the first to create one!
        </div>

      @endif

      @if (Auth::check())

        <a class="button is-primary" href="/threads/{{ $game->id }}/new">Create a new thread</a>

      @endif

    </div>
  </div>

@endsection
