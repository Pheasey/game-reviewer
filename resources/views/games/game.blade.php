<a href="/games/{{ $game->id }}" class="panel-block">
  <span class="panel-icon">
    <i class="fa fa-gamepad"></i>
  </span>
  {{ $game->title }}
  <br>
  <p>released {{ $game->formattedDate() }}</p>
</a>
