@extends('layout')

@section('content')

  <h3 class="is-title">Upcoming Games</h3>

  <nav class="panel">

    @foreach ($games as $game)

      @include('games.game')

    @endforeach

  </nav>

  <h3 class="is-title">Recent Releases</h3>

  <nav class="panel">

    @foreach ($games as $game)

      @include('games.game')

    @endforeach

  </nav>

  {{-- @include('layouts.jumbotron')

  @include('layouts.gallery')

  @include('layouts.twocolnews')

  @include('layouts.forum') --}}

@endsection
