@extends('layout')

@section('content')

  <h3 class="is-title">Update your review for {{ $game->title }}</h3>

  <form method="POST" action="/games/{{ $game->id }}/review/update">

      {{ csrf_field() }}
      {{ method_field('PUT') }}

      <input type="hidden" id="game_id" name="game_id" value="{{ $game->id }}">
      <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">

      <label class="label">Star Rating <small>0/10</small></label>
      <p class="control">
        <input class="input" id="rating" name="rating" type="number" min="0" max="10" value="">
      </p>

      <label class="label">Review</label>
      <p class="control">
        <textarea class="textarea" id="review" name="review"></textarea>
      </p>

      <p class="control">
        <button class="button is-primary">Update</button>
      </p>

  </form>


@endsection
