@extends('layout')

@section('content')

  <h3 class="is-title">All Games</h3>

  <nav class="panel">

    @foreach ($games as $game)

      @include('games.game')

    @endforeach

  </nav>

  {{ $games->links() }}

  @if (Auth::user() && (Auth::user()->role->name == "Admin"))
    <a class="button" href="/games/new">Add a new Game</a>
  @endif

@endsection
