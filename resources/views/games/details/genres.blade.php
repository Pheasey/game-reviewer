@if ($game->genres)

  <div class="tags">

    @foreach ($game->genres as $genre)

      <a href="#">{{ $genre->name }}</a>

    @endforeach

  </div>

@endif
