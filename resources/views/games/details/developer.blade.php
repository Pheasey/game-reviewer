@if ($game->devs)

  <p style="margin: 0;">
    <strong>Developer:</strong> <a href="/devs/{{ $game->dev->id }}">{{ $game->dev->name }}</a>
    <br>
  </p>

@endif
