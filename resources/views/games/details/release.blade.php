@if ($game->release_date)

  <p style="margin: 0;">
    <strong>Released:</strong> {{ $game->formattedDate() }}
    <br>
  </p>

@endif
