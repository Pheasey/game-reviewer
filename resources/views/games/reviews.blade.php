@if (!$game->reviews())

  <p>No reviews for this game yet.</p>

@else

  <p>There's a total of {{ $game->reviews()->count() }} reviews for this game.</p>

  <ul>

      @foreach ($game->reviews as $review)

      <li>
        Rating: {{ $review->pivot->rating/2 }}/5 - {{ $review->pivot->review }} by
        <a href="/users/{{$review->pivot->user_id}}">{{ App\User::find($review->pivot->user_id)->name }}</a>
      </li>

      @endforeach

  </ul>

@endif
