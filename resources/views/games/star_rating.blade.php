
@if ($game->reviews())

  <div class="ratings">

    @if (!$game->getRating())

      @for ($i=0; $i < 5; $i++)

        <i class="fa fa-star-o" aria-hidden="true"></i>

      @endfor

    @else

      @for ($i=0; $i < floor($game->getRating()); $i++)

        <i class="fa fa-star" aria-hidden="true"></i>

      @endfor

      @if (fmod($game->getRating(), 1) > 0)

        <i class="fa fa-star-half-o" aria-hidden="true"></i>

      @endif

      @for ($i=0; $i < (4 - floor($game->getRating())); $i++)

        <i class="fa fa-star-o" aria-hidden="true"></i>

      @endfor

    @endif

    <i> ({{ round($game->getRating(), 2) }} / {{ $game->getVoteCount() }} votes)</i>

  </div>

@else

  <p>No reviews yet.</p>

@endif
