@extends('layout')

@section('content')

  <h3 class="is-title">Games developed by {{ $dev->name }}</h2>

  {{-- Headquarters: {{ $dev->headquarters }}
  Founded: {{ $dev->founded }}
  Website: <a href="{{ $dev->website }}">{{ $dev->website }}</a> --}}

  <nav class="panel">

    @foreach ($dev->games as $game)
      @include('games.game')
    @endforeach

  </nav>


@endsection
