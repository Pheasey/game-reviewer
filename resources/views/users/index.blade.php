@extends('layout')

@section('content')

  <h1 class="is-title">All Users</h1>

  <div class="panel">

    @foreach ($users as $user)

      @include('users.user')

    @endforeach

  </div>

  {{-- <div class="panel-block">
    {{ $users->links() }}
  </div> --}}

@endsection
