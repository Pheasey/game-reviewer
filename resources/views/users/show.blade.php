@extends('layout')

@section('content')

  <h3 class="is-title is-3">{{ $user->name }}'s User Profile

    @if ($user->role->name != "Default")
      <span class="tag is-medium">{{ $user->role->name }}</span>
    @endif

  </h3>

  <div class="panel">

    @include('users.user')

  </div>

  @include('users.social')

  @if (Auth::user() && Auth::user() == $user)
    <a href="/users/{{$user->id}}/edit" class="button is-pulled-right">Edit My Details</a>
  @elseif (Auth::user() && Auth::user()->role->name == "Admin")
    <a href="/users/{{$user->id}}/edit" class="button is-pulled-right">Edit {{ $user->name }}'s Details</a>
  @endif

  @include('users.details')

@endsection
