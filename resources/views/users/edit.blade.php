@extends('layout')

@section('content')

  <h3 class="is-title">Editing {{ $user->name }}'s Details</h3>

  <div class="panel">

    @include('users.user')

  </div>

  <form method="POST" action="/users/{{ $user->id }}/update">

    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <p class="title is-4">General Info</p>
    <p class="subtitle is-6">Only your username is public.</p>

    <label class="label">Username</label>
    <p class="control">
      <input class="input" id="name" name="name" type="text" value="{{ $user->name }}">
    </p>

    <label class="label">Email</label>
    <p class="control">
      <input class="input" id="email" name="email" type="text" value="{{ $user->email }}">
    </p>

    <label class="label">Date of Birth</label>
    <p class="control">
      <input class="input" type="date" id="dob" name="dob" value="{{ $user->dobFormatted() }}"></textarea>
    </p>

    <br>
    <p class="title is-4">Social Links</p>
    <p class="subtitle is-6">These will be public on your profile.</p>

    <label class="label">Discord ID</label>
    <p class="control">
      <input class="input" id="discord" name="discord" type="text" placeholder="[name#number]" value="{{ $user->discord }}">
    </p>

    <label class="label">Twitter</label>
    <p class="control">
      <input class="input" id="twitter" name="twitter" type="text" placeholder="@[handle]" value="{{ $user->twitter }}">
    </p>

    <label class="label">Twitch</label>
    <p class="control">
      <input class="input" id="twitch" name="twitch" type="text" placeholder="twitch.tv/[username]" value="{{ $user->twitch }}">
    </p>

    <label class="label">YouTube</label>
    <p class="control">
      <input class="input" id="youtube" name="youtube" type="text" placeholder="channel/[username]" value="{{ $user->youtube }}">
    </p>

    <p class="control">
      <button class="button is-primary">Update</button>
    </p>

  </form>

  @include('layouts.errors')

@endsection
