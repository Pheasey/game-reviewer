@if ($user->discord || $user->twitter || $user->twitch || $user->youtube)

  <div class="panel">
    <div class="panel-block">

      @if ($user->discord)

        <i class="fa fa-comments" aria-hidden="true"></i> &nbsp;
        <b>Discord: &nbsp;</b> <a href="#">{{ $user->discord }}</a> &nbsp;&nbsp;

      @endif

      @if ($user->twitter)

        <i class="fa fa-twitter" aria-hidden="true"></i> &nbsp;
        <b>Twitter: &nbsp;</b> @<a href="https://twitter.com/{{ $user->twitter }}">{{ $user->twitter }}</a> &nbsp;&nbsp;

      @endif

      @if ($user->twitch)

        <i class="fa fa-twitch" aria-hidden="true"></i> &nbsp;
        <b>Twitch: &nbsp;</b> <a href="https://www.twitch.tv/{{ $user->twitch }}">{{ $user->twitch }}</a> &nbsp;&nbsp;

      @endif

      @if ($user->youtube)

        <i class="fa fa-youtube-play" aria-hidden="true"></i> &nbsp;
        <b>YouTube: &nbsp;</b> <a href="https://www.youtube.com/channel//{{ $user->youtube }}">{{ $user->youtube }}</a> &nbsp;&nbsp;

      @endif

    </div>
  </div>

@endif
