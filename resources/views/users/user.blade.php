<div class="panel-block">
  <article class="media">
    <figure class="media-left">
      <p class="image is-64x64">
        <img src="https://robohash.org/{{ $user->id }}.png">
      </p>
    </figure>
    <div class="media-content">
      <div class="content">
        <p>
          <strong><a href="/users/{{ $user->id }}">{{ $user->name }}</a></strong> <small>id#{{ $user->id }}</small>
          <br>
          Email:
          @if (Auth::check() && (Auth::user()->role->name == "Admin" || Auth::user() == $user))
            {{ $user->email }}

          @else
            <i>Private Email Address</i>
          @endif
          <br>
          Joined: {{ $user->created_at->diffForHumans() }}
        </p>
      </div>
    </div>
  </article>
</div>
