<h3 class="is-title">Activity Feed</h3>
<h4 class="is-title">Recent Threads <small>({{ $user->threads->count() }})</small></h4>

@if ($user->threads)

  <ul>

    @foreach ($user->threads as $thread)

      <li><a href="/threads/{{$thread->id}}">{{ $thread->title}}</a> on the game <a href="/games/{{$thread->game->id}}">{{$thread->game->title}}</a></li>

    @endforeach

@endif

@if ($user->threads->count() == 0)

  <li>No threads by this user :(</li>

@endif

</ul>


<h4 class="is-title">Recent Posts <small>({{ $user->posts->count() }})</small></h4>

@if ($user->posts)

  <ul>

  @foreach ($user->posts as $post)

    <li>
      {{ substr($post->body, 0, 40) }}.. on <a href="/threads/{{ $post->thread->id }}">{{ $post->thread->title }}</a>
    </li>

  @endforeach

@endif

@if ($user->posts->count() == 0)

  <li>No posts by this user :(</li>

@endif

</ul>


<h4 class="is-title">Recent Reviews <small>({{ $user->reviews->count() }})</small></h4>

@if ($user->reviews)

  <ul>

    @foreach ($user->reviews as $review)

      <li>

        <a href="/games/{{ $review->id }}">{{ $review->title }}</a> - {{ $review->pivot->rating/2 }}/5 - {{ $review->pivot->review }}

      </li>

    @endforeach

@endif

@if ($user->reviews->count() == 0)

  <li>No reviews by this user :(</li>

@endif

</ul>
