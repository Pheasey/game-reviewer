@extends('layout')

@section('content')

  <h3 class="is-title">Updating Post</h3>

  <form method="POST" action="/posts/{{ $post->id }}/update">

    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">

    <label class="label">Reply</label>
    <p class="control">
      <textarea class="textarea" id="body" name="body">{{ $post->body }}</textarea>
    </p>

    <p class="control">
      <button class="button is-primary">Update Post</button>
    </p>

  </form>

  @include('layouts.errors')

@endsection
