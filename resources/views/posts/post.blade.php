<article class="media">
  <figure class="media-left">
    <p class="image is-64x64">
      <img src="https://robohash.org/{{ $post->user->id }}.png">
    </p>
  </figure>
  <div class="media-content">
    <div class="content">
      <p>
        @if (Auth::user() && (Auth::id() == $post->user->id) && $post->hidden != true && $post->thread->locked != true)
          <a class="button is-pulled-right" href="/posts/{{ $post->id }}/edit">Edit</a>
        @endif
        <strong>Reply from <a href="/users/{{ $post->user->id }}">{{ $post->user->name }}</a> <i>{{ $post->created_at->diffForHumans() }}</i></strong>
        <br>

        @if (!$post->hidden)
          {!! Markdown::parse($post->body) !!}
        @else
          <p><i>This post was hidden.</i></p>
        @endif

        @if (Auth::user() && (Auth::user()->role->name == "Admin"))

          <form action="/posts/{{ $post->id }}/hide" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <button class="button is-primary delete is-pulled-right"></button>
          </form>

        @endif

      </p>
    </div>
  </div>
</article>
