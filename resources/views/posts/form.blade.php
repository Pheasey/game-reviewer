<br>
<form method="POST" action="/posts/{{$thread->id}}/new">

  {{ csrf_field() }}

  <input type="hidden" id="thread_id" name="thread_id" value="{{ $thread->id }}">
  <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">

  <label class="label">Reply</label>
  <p class="control">
    <textarea class="textarea" id="body" name="body"></textarea>
  </p>

  <p class="control">
    <button class="button is-primary">Post Reply</button>
  </p>

</form>

@include('layouts.errors')
