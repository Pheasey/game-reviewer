<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Game Reviews</title>
		{{-- <link rel="stylesheet" href="/css/hydra.css"> --}}
		@yield('head')
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.3.1/css/bulma.css" />
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Roboto:300,400" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/css/style.min.css" >
</head>
	<body>
		<div class="container">

	    @include('layouts.nav')

				<!-- Cols -->
	    <div class="columns">
	      <div class="column is-two-thirds">
					<div class="content section-l">
						@yield('content')
					</div>
				</div>

				<div class="column">
					<div class="content section-r">
						<h3 class="is-title is-3">Upcoming Games</h3>

						@include('layouts.search')

						@include('layouts.sidebar')

					</div>
				</div>
			</div>

			@include('layouts.footer')

		</div>
	</body>
</html>
