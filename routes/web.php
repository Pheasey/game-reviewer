<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'SessionController@destroy');

Route::get('/', 'NewsController@index');
Route::get('/home', 'NewsController@index');

// Games
Route::get('games/search', 'GamesController@search');

Route::get('/games', 'GamesController@index');
Route::get('/games/all', 'GamesController@indexAll');
Route::get('/games/calendar', 'GamesController@calendar');
Route::get('games/new', 'GamesController@newForm');
Route::get('/games/{game}', 'GamesController@show');
Route::post('games/{game}/review/new', 'GamesController@newReview');
Route::get('games/{game}/review/update', 'GamesController@edit');
Route::put('games/{game}/review/update', 'GamesController@update');
Route::post('games/create', 'GamesController@newGame');

// Threads
Route::get('/threads', 'ThreadController@index');
Route::get('/threads/{game}/new', 'ThreadController@form');
Route::post('/threads/{game}/new', 'ThreadController@newThread');
Route::get('/threads/{thread}', 'ThreadController@show');

Route::put('/threads/{thread}/pin', 'ThreadController@pin');
Route::put('/threads/{thread}/lock', 'ThreadController@lock');
Route::put('/threads/{thread}/hide', 'ThreadController@hide');

Route::get('/threads/{thread}/edit', 'ThreadController@updateForm');
Route::put('/threads/{thread}/update', 'ThreadController@update');

// Posts
Route::get('/posts/{thread}/new', 'PostController@form');
Route::post('/posts/{thread}/new', 'PostController@newPost');
Route::put('/posts/{post}/hide', 'PostController@hide');

Route::get('/posts/{post}/edit', 'PostController@updateForm');
Route::put('/posts/{post}/update', 'PostController@update');

// Developers
Route::get('/devs/{dev}', 'DevController@show');

// Users
Route::get('/users', 'UserController@index');
Route::get('users/{user}', 'UserController@show');
Route::get('users/{user}/edit', 'UserController@edit');
Route::put('users/{user}/update', 'UserController@update');

// TODO Fix edit page link.

// News
Route::get('/news', 'NewsController@index');
Route::get('news/create', 'NewsController@form');
Route::post('news/create', 'NewsController@newNews');
Route::get('news/{news}', 'NewsController@show');

Route::get('news/{news}/edit', 'NewsController@edit');
Route::put('news/{news}', 'NewsController@update');

// API
Route::get('api/game/{game}', 'APIController@game');
Route::get('api/thread/{thread}', 'APIController@thread');
Route::get('api/thread/{thread}/posts', 'APIController@posts');
Route::get('api/games/upcoming', 'APIController@upcoming');
