<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Thread;
use App\Game;

class ThreadController extends Controller
{
    public function index() {
        $threads = Thread::latest()->paginate(15);
        return view('threads.index', compact('threads'));
    }

    public function show(Thread $thread) {
      return view('threads.show', compact('thread'));
    }

    public function form(Game $game) {
      return view('threads.form', compact('game'));
    }

    public function newThread(Request $request) {
      $this->validate($request, [
            'title' => 'required|min:10',
            'body' => 'required|min:10',
            'user_id' => 'required',
            'game_id' => 'required',
        ]);

        $thread = new Thread();
        $thread->title = $request['title'];
        $thread->body = $request['body'];
        $thread->user_id = $request['user_id'];
        $thread->game_id = $request['game_id'];
        $thread->save();

        return redirect("/games/{$request['game_id']}");
    }

    public function pin(Request $request, Thread $thread) {
        if ($thread->pinned == true) {
            $thread->pinned = false;
        } else {
            $thread->pinned = true;
        }
        $thread->save();
        return redirect("/threads/{$thread->id}");
    }

    public function lock(Request $request, Thread $thread) {
        if ($thread->locked == true) {
            $thread->locked = false;
        } else {
            $thread->locked = true;
        }
        $thread->save();
        return redirect("/threads/{$thread->id}");
    }

    public function hide(Request $request, Thread $thread) {
        if ($thread->hidden == true) {
            $thread->hidden = false;
        } else {
            $thread->hidden = true;
        }
        $thread->save();
        return redirect("/threads/{$thread->id}");
    }

    public function updateForm(Thread $thread) {
        return view('threads.update_form', compact('thread'));
    }

    public function update(Request $request, Thread $thread) {
        $thread = Thread::find($thread->id);
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);

        $thread->title = $request['title'];
        $thread->body = $request['body'];
        $thread->save();

        session()->flash('message', 'Thread was successfully updated.');
        return redirect("/threads/{$thread->id}");
    }

}
