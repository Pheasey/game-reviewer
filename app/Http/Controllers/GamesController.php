<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use Carbon\Carbon;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use DB;

class GamesController extends Controller
{
    public function index() {
        $games = Game::all();
        return view('games.index', compact('games'));
    }

    public function show(Game $game) {
        return view('games.show', compact('game'));
    }

    public function indexAll() {
        $games = Game::latest()->orderBy('title')->paginate(10);
        return view('games.indexAll', compact('games'));
    }

    public function calendar() {
        $games = Game::all();

        $events = [];

        $events[] = \Calendar::event(
            "Valentine's Day",
            true,
            '2017-02-14',
            '2017-02-14',
            1,
            [
                'url' => 'https://youtu.be/NNC0kIzM1Fo',
            ]
        );

        foreach ($games as $game) {
            $url = url('/games/'.$game->id);
            $events[] = \Calendar::event(
                $game->title,
                true,
                $game->release_date,
                $game->release_date,
                1,
                [
                    'url' => $url,
                ]
            );
        }

        $calendar = Calendar::addEvents($events) //add an array with addEvents
            ->setOptions([ //set fullcalendar options
                'firstDay' => 1
            ]);

        $lastMonth = Game::where('release_date', '>=', Carbon::now()->subMonth())->orderBy('release_date')->get();
        $thisMonth = Game::where( DB::raw('MONTH(release_date)'), '=', date('n') )->orderBy('release_date')->get();

        return view ('games.calendar', compact('calendar', 'lastMonth', 'thisMonth'));
    }

    public function search(Request $request) {
      $query = \Request::get('term');
      $games = Game::where('title', 'LIKE', '%' . $query . '%')->orderBy('id')->paginate(5);
      return view('games.results', compact('games', 'query'));
    }

    public function newReview(Request $request) {
        $this->validate($request, [
            'rating' => 'required|numeric',
            'user_id' => 'required',
        ]);
        $game = Game::find($request['game_id']);
        $game->reviews()->attach($request['user_id'], ['rating' => $request['rating'], 'review' => $request['review']]);
        return redirect("/games/{$game->id}");
    }

    public function edit(Game $game) {
        return view('games.update_review_form', compact('game'));
    }

    public function update(Game $game) {
        // validate
        // attach
        // save

        session()->flash('message', 'Review was successfully updated.');
        return redirect("/games/{$game->id}");
    }

    public function newForm() {
        return view('games.form');
    }

    public function newGame(Request $request) {
        $this->validate($request, [
          'title' => 'required',
          'release_date' => 'required',
          'website' => 'required',
          'youtube' => 'required',
        ]);

        $game = new Game();
        $game->title = $request['title'];
        $game->release_date = $request['release_date'];
        $game->youtube = $request['youtube'];
        $game->website = $request['website'];
        $game->save();

        return redirect("/games/{$game->id}");
    }

}
