<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dev;

class DevController extends Controller
{
    public function show(Dev $dev) {
      return view('devs.show', compact('dev'));
    }
}
