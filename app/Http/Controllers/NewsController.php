<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function index() {
        $news = News::latest()->paginate(3);
        return view('news.index', compact('news'));
    }

    public function show(News $news) {
        return view('news.show', compact('news'));
    }

    public function form() {
        return view('news.form');
    }

    public function newNews(Request $request) {
      $this->validate($request, [
            'title' => 'required|min:5',
            'body' => 'required|min:10',
            'user_id' => 'required',
        ]);

        $news = new News();
        $news->title = $request['title'];
        $news->body = $request['body'];
        $news->user_id = $request['user_id'];
        $news->save();

        return redirect("/news/{$news->id}");
    }

    public function edit(News $news) {
        return view('news.update_form', compact('news'));
    }

    public function update(Request $request, News $news) {
        $news = News::find($news->id);
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);

        $news = News::find($news->id);

        $news->title = $request['title'];
        $news->body = $request['body'];
        $news->save();

        session()->flash('message', 'Article was successfully updated.');
        return redirect("/news/{$news->id}");
    }

}
