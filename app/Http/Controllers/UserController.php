<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index() {
      $users = User::get();
      return view('users.index', compact('users'));
    }

    public function show(User $user) {
      return view('users.show', compact('user'));
    }

    public function edit(User $user) {
      return view('users.edit', compact('user'));
    }

    public function update(Request $request, User $user) {
      $this->validate($request, [
          'name' => 'required',
          'email' => 'required',
          'dob' => 'required',
      ]);

      $user->name = $request['name'];
      $user->email = $request['email'];
      $user->dob = $request['dob'];

      $user->discord = $request['discord'];
      $user->twitter = $request['twitter'];
      $user->twitch = $request['twitch'];
      $user->youtube = $request['youtube'];

      $user->save();

      session()->flash('message', 'Details successfully updated.');
      return redirect("/users/{$user->id}");

    }
}
