<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function form(Game $game) {
      return view('posts.form', compact('game'));
    }

    public function newPost(Request $request) {
      $this->validate($request, [
            'body' => 'required|min:10',
            'user_id' => 'required',
            'thread_id' => 'required',
        ]);

        $post = new Post();
        $post->body = $request['body'];
        $post->user_id = $request['user_id'];
        $post->thread_id = $request['thread_id'];
        $post->save();

        return redirect("/threads/{$request['thread_id']}");
    }

    public function hide(Request $request, Post $post) {
        $post = Post::find($post->id);
        if ($post->hidden == true) {
          $post->hidden = false;
        } else {
          $post->hidden = true;
        }
        $post->save();
        return redirect("/threads/{$post->thread->id}");
    }

    public function updateForm(Post $post) {
        return view('posts.update_form', compact('post'));
    }

    public function update(Request $request, Post $post) {
        $post = Post::find($post->id);
        $this->validate($request, [
            'body' => 'required',
        ]);

        $post->body = $request['body'];
        $post->save();

        session()->flash('message', 'Post was successfully updated.');
        return redirect("/threads/{$post->thread->id}");
    }
}
