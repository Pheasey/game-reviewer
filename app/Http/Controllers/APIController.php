<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Game;
use App\Thread;
use DB;

class APIController extends Controller
{

    public function game(Game $game) {
        return $game;
    }

    public function thread(Thread $thread) {
        return $thread;
    }

    public function posts(Thread $thread) {
        return $thread->posts;
    }

    public function upcoming() {
        $thisMonth = Game::where( DB::raw('MONTH(release_date)'), '=', date('n') )->get();
        return $thisMonth;
    }

}
