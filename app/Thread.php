<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Thread extends Model
{

    protected $fillable = ['title', 'body', 'user_id', 'game_id'];

    public function posts() {
        return $this->hasMany(Post::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function game() {
        return $this->belongsTo(Game::class);
    }

    public static function sideContent() {
        $recentThreads = Thread::where( DB::raw('MONTH(created_at)'), '=', date('n') )->orderBy('created_at')->limit(5)->get();
        return $recentThreads;
    }
}
