<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Game;

class Dev extends Model
{
    public function games() {
        return $this->hasMany(Game::class);
    }
}
