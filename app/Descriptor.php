<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descriptor extends Model
{
    public function games() {
        return $this->belongsToMany(Game::class);
    }
}
