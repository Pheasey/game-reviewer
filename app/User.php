<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function threads() {
        return $this->hasMany(Thread::class);
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }

    public function games() {
        return $this->belongsToMany(Game::class)->withPivot('rating');
    }

    public function reviews() {
        return $this->belongsToMany(Game::class, 'game_user')->withPivot('rating', 'review');
    }

    public function news() {
        return $this->hasMany(News::class);
    }

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function dobFormatted() {
        $date = Carbon::parse($this->dob);
        $date->setToStringFormat('Y-m-d');
        return $date;
    }

    public function getReview($game_id) {
        return $this->reviews->where('game_id', '=', $game_id);
    }

}
