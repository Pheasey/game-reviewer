<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Game extends Model
{
    public function dev() {
        return $this->belongsTo(Dev::class);
    }

    public function threads() {
        return $this->hasMany(Thread::class);
    }

    public function platforms() {
        return $this->belongsToMany(Platform::class);
    }

    public function genres() {
        return $this->belongsToMany(Genre::class, 'game_genres');
    }

    public function age_rating() {
        return $this->belongsTo(Age_Rating::class);
    }

    public function descriptor() {
        return $this->belongsToMany(Descriptor::class);
    }

    public function users() {
        return $this->belongsToMany(User::class);
    }

    public function reviews() {
        return $this->belongsToMany(User::class, 'game_user')->withPivot('rating', 'review');
    }

    public function getRating() {
        return $this->reviews()->avg('rating')/2;
    }

    public function getVoteCount() {
        return $this->reviews()->count();
    }

    public function formattedDate() {
        $date = Carbon::parse($this->release_date);
        $date->setToStringFormat('F jS Y');
        return $date;
    }

    // public static function sideContent() {
    //     $recentGames = DB::table('games')
    //     ->where('release_date', '<=', Carbon::now())
    //     ->orderBy('release_date', 'desc')->limit(3);
    //     return $recentGames;
    // }

    public static function sideContent() {
        $recentGames = Game::where( DB::raw('MONTH(release_date)'), '=', date('n') )->orderBy('release_date')->limit(5)->get();
        return $recentGames;
    }

}
