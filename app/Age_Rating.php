<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Age_Rating extends Model
{
    public function games() {
        return $this->hasMany(Game::class);
    }
}
