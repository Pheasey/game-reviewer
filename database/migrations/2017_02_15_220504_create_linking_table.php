<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descriptor_game', function (Blueprint $table) {
            $table->integer('descriptor_id');
            $table->integer('game_id');
            $table->primary(['descriptor_id', 'game_id']);
            $table->timestamps();
        });

        Schema::create('game_platform', function (Blueprint $table) {
            $table->integer('game_id');
            $table->integer('platform_id');
            $table->primary(['game_id', 'platform_id']);
            $table->timestamps();
        });

        Schema::create('game_genres', function (Blueprint $table) {
            $table->integer('game_id');
            $table->integer('genre_id');
            $table->primary(['game_id', 'genre_id']);
            $table->timestamps();
        });

        Schema::create('game_user', function (Blueprint $table) {
            $table->integer('game_id');
            $table->integer('user_id');
            $table->integer('rating');
            $table->text('review')->nullable();
            $table->primary(['game_id', 'user_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('descriptor_game');
          Schema::dropIfExists('game_platform');
          Schema::dropIfExists('game_genres');
          Schema::dropIfExists('game_user');
    }
}
