<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('games')->insert([
          'title' => 'Fifa 17',
          'release_date' => '2016-09-27',
          'website' => 'https://www.easports.com/fifa',
          'youtube' => '-3fjoe5Njpc',
          'dev_id' => 1,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('games')->insert([
          'title' => 'The Legend of Zelda™: Breath of the Wild',
          'release_date' => '2017-03-03',
          'website' => 'http://www.zelda.com/breath-of-the-wild/',
          'youtube' => 'zw47_q9wbBE',
          'dev_id' => 5,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('games')->insert([
          'title' => 'Horizon Zero Dawn',
          'release_date' => '2017-02-28',
          'website' => 'https://www.playstation.com/en-gb/games/horizon-zero-dawn-ps4/',
          'youtube' => 'T5Xx3MdqdgM',
          'dev_id' => 3,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('games')->insert([
          'title' => 'Tom Clancy\'s Ghost Recon: Wildlands',
          'release_date' => '2017-03-07',
          'website' => 'http://ghost-recon.ubisoft.com/wildlands/en-gb/home/',
          'youtube' => 'WdJub3Kz2wI',
          'dev_id' => 2,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('games')->insert([
          'title' => 'Mario Sports Superstars',
          'release_date' => '2017-03-24',
          'website' => 'http://www.nintendo.com/games/detail/mario-sports-superstars-3ds',
          'youtube' => '_j7SNjlsSDc',
          'dev_id' => 6,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('games')->insert([
          'title' => 'Mass Effect: Andromeda',
          'release_date' => '2017-03-21',
          'website' => 'https://www.masseffect.com/',
          'youtube' => 'HrWgLMH8yRU',
          'dev_id' => 4,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('games')->insert([
          'title' => 'Star Trek: Bridge Crew',
          'release_date' => '2017-03-14',
          'website' => 'https://www.ubisoft.com/en-GB/game/star-trek-bridge-crew',
          'youtube' => 'romB8e5nMp8',
          'dev_id' => 7,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // Game genress
        DB::table('game_genres')->insert([
            'game_id' => 1,
            'genre_id' => 1,
        ]);

        DB::table('game_genres')->insert([
            'game_id' => 1,
            'genre_id' => 7,
        ]);

        DB::table('game_genres')->insert([
            'game_id' => 1,
            'genre_id' => 10,
        ]);

        DB::table('game_genres')->insert([
            'game_id' => 2,
            'genre_id' => 1,
        ]);

        // Threads

        DB::table('threads')->insert([
            'game_id' => 1,
            'user_id' => 1,
            'title' => 'This is a test thread',
            'body' => 'This is just a thread I made to see if threads are working!',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // Posts

        DB::table('posts')->insert([
            'thread_id' => 1,
            'user_id' => 1,
            'body' => 'This is a great thread, so great I made a post!',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // News

        DB::table('news')->insert([
            'user_id' => 1,
            'title' => 'This is news..',
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // Descriptors
        DB::table('descriptors')->insert([
            'name' => 'Bad Language',
            'description' => 'Game contains bad language.',
            'image_path' => 'bad-language.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('descriptors')->insert([
            'name' => 'Discrimination',
            'description' => 'Game contains depictions of, or material which may...',
            'image_path' => 'discrimination.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('descriptors')->insert([
            'name' => 'Drugs',
            'description' => 'Game refers to or depicts the use of drugs.',
            'image_path' => 'drugs.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('descriptors')->insert([
            'name' => 'Fear',
            'description' => 'Game may be frightening or scary for young childre...',
            'image_path' => 'fear.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('descriptors')->insert([
            'name' => 'Gambling',
            'description' => 'Games that encourage or teach gambling.',
            'image_path' => 'gambling.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('descriptors')->insert([
            'name' => 'Sex',
            'description' => 'Game depicts nudity and/or sexual behaviour or sex...',
            'image_path' => 'sex.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('descriptors')->insert([
            'name' => 'Violence',
            'description' => 'Game contains depictions of violence.',
            'image_path' => 'violence.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('descriptors')->insert([
            'name' => 'Online gameplay',
            'description' => 'Game can be played online.',
            'image_path' => 'online-gameplay.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // age__ratings
        DB::table('age__ratings')->insert([
            'name' => 'PEGI 3',
            'age_requirement' => '2003',
            'description' => 'The content of games given this rating is considered suitable for all age groups. Some violence in a comical context (typically Bugs Bunny or Tom & Jerry cartoon-like forms of violence) is acceptable. The child should not be able to associate the character on the screen with real life characters, they should be totally fantasy. The game should not contain any sounds or pictures that are likely to scare or frighten young children. No bad language should be heard.',
            'image_path' => 'Pegi_3+.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('age__ratings')->insert([
            'name' => 'PEGI 7',
            'age_requirement' => '2007',
            'description' => 'Any game that would normally be rated at 3 but contains some possibly frightening scenes or sounds may be considered suitable in this category. ',
            'image_path' => 'Pegi_7+.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('age__ratings')->insert([
            'name' => 'PEGI 12',
            'age_requirement' => '2012',
            'description' => 'Videogames that show violence of a slightly more graphic nature towards fantasy character and/or non graphic violence towards human-looking characters or recognisable animals, as well as videogames that show nudity of a slightly more graphic nature would fall in this age category. Any bad language in this category must be mild and fall short of sexual expletives.',
            'image_path' => 'Pegi_12+.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('age__ratings')->insert([
            'name' => 'PEGI 16',
            'age_requirement' => '2016',
            'description' => 'his rating is applied once the depiction of violence (or sexual activity) reaches a stage that looks the same as would be expected in real life. More extreme bad language, the concept of the use of tobacco and drugs and the depiction of criminal activities can be content of games that are rated 16.',
            'image_path' => 'Pegi_16+.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('age__ratings')->insert([
            'name' => 'PEGI 18',
            'age_requirement' => '2018',
            'description' => 'The adult classification is applied when the level of violence reaches a stage where it becomes a depiction of gross violence and/or includes elements of specific types of violence. Gross violence is the most difficult to define since it can be very subjective in many cases, but in general terms it can be classed as the depictions of violence that would make the viewer feel a sense of revulsion.',
            'image_path' => 'Pegi_18+.gif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // genress
        DB::table('genres')->insert([
            'name' => 'Action',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'Fighting',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'Action-Adventure',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'Adventure',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'Role-Playing',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'Simulation',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'Sport',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'MMO',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'Strategy',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genres')->insert([
            'name' => 'Online',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('roles')->insert([
            'name' => 'Default',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('roles')->insert([
            'name' => 'Admin',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // Platforms
        DB::table('platforms')->insert([
            'name' => 'PS4',
            'image_path' => 'ps4.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('platforms')->insert([
            'name' => 'Xbox',
            'image_path' => 'xbox.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('platforms')->insert([
            'name' => 'Steam',
            'image_path' => 'steam.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('platforms')->insert([
            'name' => 'Switch',
            'image_path' => 'switch.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('platforms')->insert([
            'name' => 'Apple',
            'image_path' => 'apple.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('platforms')->insert([
            'name' => 'Android',
            'image_path' => 'android.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('platforms')->insert([
            'name' => 'PlayStation',
            'image_path' => 'playstation.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);


        // Game Platform
        DB::table('game_platform')->insert([
            'game_id' => 1,
            'platform_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('game_platform')->insert([
            'game_id' => 1,
            'platform_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('game_platform')->insert([
            'game_id' => 1,
            'platform_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('game_platform')->insert([
            'game_id' => 2,
            'platform_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('game_platform')->insert([
            'game_id' => 3,
            'platform_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('game_platform')->insert([
            'game_id' => 4,
            'platform_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('game_platform')->insert([
            'game_id' => 4,
            'platform_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('game_platform')->insert([
            'game_id' => 4,
            'platform_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // Developers
        DB::table('devs')->insert([
            'name' => 'EA Canada',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('devs')->insert([
            'name' => 'Ubisoft Paris',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('devs')->insert([
            'name' => 'Guerrilla Games',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('devs')->insert([
            'name' => 'BioWare',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('devs')->insert([
            'name' => 'Monolith Soft',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('devs')->insert([
            'name' => 'Camelot Software Planning',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('devs')->insert([
            'name' => 'Red Storm Entertainment',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
